# American Reading Company #
## Web Designer Assessment ##

This assessment is designed to test the technical skills of prospective web developers. To get started, download this
repository to your computer. You can do this in one of three ways:

* Download a Zip archive of this repository using the download link on the left. Submit your finished assessment by emailing a Zip archive of the finished files to hr@americanreading.com.
* Clone this repository. Submit your finished assessment by emailing a Zip archive of the finished files to hr@americanreading.com.
* **Extra Credit:** Fork this repository. Submit your finished assessment via a pull request.

You can use any tools you wish to complete this assessment. Feel free to use the Internet to look for resources!

## Task: Fix an Old Website ##

We have an old website that sorely needs to be updated. It's called "Pog Universe." Hey, we said it was old! Are there 
any errors on this website that you can fix? Can you update the site to be a little more modern?

```
Directory: /task-1-pogs
```
